﻿using System;
using System.Dynamic;

namespace SimpleConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type your name:");
            string name = Console.ReadLine();
            Console.WriteLine("Hello " + name + ", your name is " + name.Length + " characters long and starts with " + name.Substring(0,1) + ".");
        }
    }
}   
